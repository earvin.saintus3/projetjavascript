Ce dossier comportera chaque projet que je vais réaliser au cours de ma certification avec Dyma.fr

Projet 1: Ce premier projet est une TodoList
- ajouter un élément dans une liste,
- afficher la liste
- supprimer un élémenet de la liste
- valider une todo
- editer une todo

Avec les notions du DOM, EVENTS, TABLEAU, CLOSURE, OBJET, FONCTIONS

Projet 2: Ce deuxième projet est un blog créer en 
HTML, CSS, SASS, JS 
Les objectifs étaient les suivants:
- Créer plusieurs pages (plusieurs bundles WebPack)
- Mise en place de Sass
- Utilisation d'images
- Création d'articles
- Topbar responsive avec menu pour mobile

Projet 3: Ce quatrième projet est un jeu snake créer en HTML, CSS, Canvas, JS 
