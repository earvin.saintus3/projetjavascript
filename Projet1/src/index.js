const ul = document.querySelector('ul');
const form2 = document.querySelector('form > button');
const input = document.querySelector('form > input');

const todos = [
  {
    text: "Je suis une todo",
    done: false,
    editMode: true
  },
  {
    text: "Faire du JS",
    done: true,
    editMode: false
  },
  
]

form2.addEventListener("click", event => {
  event.preventDefault();
  const value = input.value;
  input.value = '';
  addTodo(value);
})

const displayTodo = () => {
  const todoNode = todos.map((todo, index) =>{
    if(todo.editMode){
      return createTodoElemEdit(todo, index);
    }else {
      return createTodoElem(todo, index);
    }
  });
  ul.innerHTML = '';
  ul.append(...todoNode);
}

const createTodoElemEdit = (todo, index) => {
  const li = document.createElement('li');
  const input = document.createElement('input');
  input.type = "text";
  input.value = todo.text;
  const btnSave = document.createElement('button');
  btnSave.innerHTML = "Save";
  const btnCancel = document.createElement('button');
  btnCancel.innerHTML = "Cancel";
  btnCancel.addEventListener('click', e => {
    e.stopPropagation();
    toggleEditMode(index);
  });
  btnSave.addEventListener('click', e => {
    editTodo(index, input);
  });
  li.append(input, btnCancel, btnSave);
  return li;
}

const createTodoElem = (todo, index) => {
  const li = document.createElement('li');
  const btnD = document.createElement('button');
  btnD.innerHTML= "Supprimer";
  const btnEdit = document.createElement('button');
  btnEdit.innerHTML= "Editer";

  btnD.addEventListener('click', (event) => {
    event.stopPropagation();
    deleteElem(index);
  });

  btnEdit.addEventListener('click', event => {
    event.stopPropagation();
    toggleEditMode(index);
  })

  li.innerHTML = `
    <span class="todo ${ todo.done ? 'done' : '' }"></span>
    <p>${ todo.text }</p>
  `;
  li.addEventListener('click', (event) => {
    toggleTodo(index);
  });
  li.append(btnEdit, btnD)
  return li;
}

const addTodo = (text) => {
  todos.push({
    text,
    done: false
  });
  displayTodo();
}

const deleteElem = (index) => {
  todos.splice(index, 1);
  displayTodo();
}

const toggleTodo = (index) => {
  todos[index].done = !todos[index].done;
  displayTodo();
}

const toggleEditMode = (index) => {
  todos[index].editMode = !todos[index].editMode;
  displayTodo();
}

const editTodo = (i,input) => {
  const value = input.value;
  todos[i].text = value;
  todos[i].editMode =false;
  displayTodo();
}

displayTodo();




